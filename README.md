# ansible-role-nextcloud

Ansible Role to install Nextcloud. (Based on ReinerNippes [work](https://github.com/ReinerNippes/nextcloud) released under MIT licence)

- [x] Nextcloud (Latest) - <https://nextcloud.com/>
- [x] Nginx - <https://nginx.org/>
- [x] PHP 7.x - <http://www.php.net/>
- [x] PostgreSQL 11 <https://www.postgresql.org/>
- [x] Redis - <https://redis.io/>
- [ ] Nextcloud Talk
- [ ] Collabora Online <https://www.collaboraoffice.com/>
- [ ] LDAP integration

Most of the settings are recommentations from C. Rieger

Visit his page for all details: <https://www.c-rieger.de/>


## Requirements

Debian 10 or Ubuntu 20.04.


## Role Variables

All variables are defined in inventory file.

```ini
# Server domain name
# Default is the fqdn of the machine
# fqdn       = nc.example.org

# selfsigned certificate as default
ssl_certificate_type  = 'selfsigned'

# Letsencrypt or selfsigned certificate
# ssl_certificate_type  = 'letsencrypt'


# Your email adresse for letsencrypt
# cert_email = nc@example.org

# receive a certificate from staging
# uncomment if you want to use letsencrypt staging environment
# cert_stage = '--staging'

#
# Nextcloud varibales

# data dir
nc_datadir           = /var/nc-data

# admin user
nc_admin             = 'admin'
nc_passwd            = ''             # leave empty to generate random password

# database settings
nc_db_type           = 'pgsql'        # (PostgreSQL)
nc_db_host           = ''
nc_db                = 'nextcloud'
nc_db_user           = 'nextcloud'
nc_db_passwd         = ''             # leave empty to generate random password
nc_db_prefix         = 'oc_'

# Nextcloud mail setup
nc_configure_mail    = false
nc_mail_from         =
nc_mail_smtpmode     = smtp
nc_mail_smtpauthtype = LOGIN
nc_mail_domain       =
nc_mail_smtpname     =
nc_mail_smtpsecure   = tls
nc_mail_smtpauth     = 1
nc_mail_smtphost     =
nc_mail_smtpport     = 587
nc_mail_smtpname     =
nc_mail_smtppwd      =

# php Version
php_version          = '7.3'

# Install turn server for Nextcloud Talk
talk_install         = false

# Allways get the latest version of Nextcloud
next_archive         = https://download.nextcloud.com/server/releases/latest.tar.bz2

# Install Collabra Online
# more info about collabora office: https://www.collaboraoffice.com/
install_collabora     = false

# Install Online Office
# more info about onlyoffice office: https://www.onlyoffice.com
install_onlyoffice    = false

#
# defaults path of your generated credentials (e.g. database, talk, onlyoffice)
credential_store      = /etc/nextcloud
```
